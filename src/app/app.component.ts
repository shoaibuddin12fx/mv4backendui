import { Component } from '@angular/core';
import { UtilityService } from './services/utility.service';
import { MessagingService } from './services/messaging.service';
import { AuthService } from './services/auth.service';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mv4backend';
  message;

  constructor(public utility: UtilityService, private messagingService: MessagingService,private authService: AuthService,private data: DataService){

  }

  ngOnInit() {
    this.messagingService.requestPermission()
    this.messagingService.receiveMessage()
    this.message = this.messagingService.currentMessage;
   }
}
