import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TabsComponent } from './tabs/tabs.component';
import { TabsComponentRoutingModule } from './tabs/tabs.component.routing.module';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'tabs',
    pathMatch: 'full'
  },
  {
    path: 'tabs',
    component: TabsComponent
  },
  
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    TabsComponentRoutingModule,
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
