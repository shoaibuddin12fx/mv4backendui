export interface CityInterface {
	  $key?: string;
	  name?: string;
	  shippingfee?: number;
	  status?: string; 
}
