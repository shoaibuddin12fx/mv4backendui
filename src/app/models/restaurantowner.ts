export interface RestaurantOwnerInterface {
  id?: string;
  address?: string;
  cardnumber?: string;
  displayName?: string;
  email?: string;
  europeResult?: string;
  facebook?: string;
  first?: string;
  language?: string;
  lastName?: string;
  lat?: string;
  lng?: string;
  nation?: string;
  ownerId?: string;
  phone?: string;
  status?: string;
}