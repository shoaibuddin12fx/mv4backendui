export interface AboutInterface {
  $key?: string;
  abouttitle?: string;
  aboutdesc?: string;
  facebook?: string;
  whatsapp?: string;
  viber?: string;
  telegram?: string;
  instagram?: string;
  youtube?: string;
  phone1?: string;
  phone2?: string;
  address?: string;
  email?: string;
  app_version?: string;
}
