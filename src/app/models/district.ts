export interface DistrictInterface {
  $key?: string;
  name?: string;
  city?: string;
  status?: string;
}
