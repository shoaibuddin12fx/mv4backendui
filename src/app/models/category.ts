export interface CategoryInterface {
	$key?:string;
	cat_id?:string;
	cat_name?:string;
	res_name?:string;
	image?:string;
	firebase_url?:string;
	status?:string;
	parent_category_id?:string;
}
