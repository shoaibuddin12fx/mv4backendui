
export interface SliderInterface {
  $key?: string;
  slider_id?: string;
  slider_name?: string;
  status?: string;
  image?: string;
  sort?: string;
  created_at?: string;
  firebase_url?: string;
  type?: string;
}
