export interface MonthReportInterface {
  id?: string;
  fee?: string;
  restaurantId?: string;
  restaurantName?: string;
  restaurantOwnerId?: string;
  total: string;
}