import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import { UserInterface } from '../models/user';
import { auth } from 'firebase/app';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private afsAuth: AngularFireAuth, private afcAuth2: AngularFireAuth,
    private afs: AngularFirestore,
    private utility: UtilityService,
    private router: Router,
  ) { }

  loginFacebookUser() {
    return this.afsAuth.signInWithPopup(new auth.FacebookAuthProvider())
      .then((credential) => this.updateUserData(credential.user));
  }

  registerUser(email: string, pass: string) {
    return new Promise((resolve, reject) => {

      this.utility.showLoader();
      var config = {
          apiKey: "AIzaSyB86_-7jSJMl8Ikaz2Gv4_61VATz3ED2_o",
          authDomain: "max-store-app.firebaseapp.com",
          databaseURL: "https://max-store-app.firebaseio.com",
          projectId: "max-store-app",
          storageBucket: "max-store-app.appspot.com",
          messagingSenderId: "997651794177",
          appId: "1:997651794177:web:f4ac216a72f0b5a280b300",
          measurementId: "G-EGFVD1MZLK"
        };
      var secondaryApp = firebase.initializeApp(config, "max-store-app");
      var self = this;
      secondaryApp.auth().createUserWithEmailAndPassword(email, pass).then(function(firebaseUser) {
          console.log("User " + firebaseUser['uid'] + " created successfully!");
          //I don't know if the next statement is necessary 
          secondaryApp.auth().signOut();


          let _email = localStorage.getItem('loginemail');
          let _pass = localStorage.getItem('loginpassword');

          console.log(_email,_pass);

          self.loginUser(_email, _pass).then( v => {
            self.utility.hideLoader();
            resolve(firebaseUser);
          })


          
      });




      // this.afcAuth2
      //   .createUserWithEmailAndPassword(email, pass)
      //   .then((userData) => {
      //     console.log(userData);
      //     resolve(userData);
      //     // this.updateUserData(userData.user);
      //   })
      //   .catch((err) => console.log(reject(err)));

    });
  }

  loginUser(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afsAuth
        .signInWithEmailAndPassword(email, pass)
        .then((userData) => {

          localStorage.setItem('loginemail', email);
          localStorage.setItem('loginpassword', pass);



          resolve(userData);
          // this.updateUserData(userData.user);

          console.log(userData);
        })
        .catch((err) => console.log(reject(err)));
    });
  }

  loginGoogleUser() {
    return this.afsAuth
      .signInWithPopup(new auth.GoogleAuthProvider())
      .then((credential) => this.updateUserData(credential.user));
  }

  async logoutUser() {
    await this.afsAuth.signOut();
    this.router.navigate(['login']);
  }

  isAuth() {
    return this.afsAuth.authState.pipe(map((auth) => auth));
  }

  checkVendor() {
    return new Promise(resolve => {
      let user = JSON.parse(localStorage.getItem('user'));
      if (user.roles.admin == true) {
        resolve(false);
      } else if (user.roles.vendor == true) {
        resolve(true);
      }
    });
  }

  getVendorKey() {
    return new Promise(resolve => {
      let user = JSON.parse(localStorage.getItem('user'));
      if (user.roles.admin == true) {
        resolve(null);
      } else if (user.roles.vendor == true) {
        resolve(user.ven_key);
      }
    });
  }

  private updateUserData(user) {
    /**
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const data: UserInterface = {
      id: user.uid,
      email: user.email,
      roles: {
        editor: true
      }
    }
    return userRef.set(data, { merge: true })

	*/
    return false;
  }

  isUserAdmin(userUid) {
    return this.afs.doc<UserInterface>(`users/${userUid}`).valueChanges();
  }
}
