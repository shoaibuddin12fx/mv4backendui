import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  public loader = false;
  constructor(
    private firebaseService: FirebaseService
  ) { }

  showLoader() {
    this.loader = true;
  }

  hideLoader() {
    this.loader = false;
  }

  presentAlert(msg) {
    Swal.fire({
      icon: 'warning',
      title: 'Failed',
      text: msg,
    });
  }

  presentSuccess(msg) {
    Swal.fire({
      icon: 'success',
      title: msg,
    });
  }

  presentConfirm() {
    return new Promise(resolve => {
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          resolve(result.value);
        }
      });
    });
  }

  presentConfirmForStatus(str) {
    return new Promise(resolve => {
      Swal.fire({
        title: 'Are you sure?',
        text: str,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.value) {
          resolve(result.value);
        }
      });
    });
  }

  getTimeDifference(time) {
    const currtime = Date.now();
    const delta = Math.abs(currtime - time) / 1000;
    if (delta > 604800) {
      const weeks = Math.floor(delta / 604800);
      return 'Nearly ' + weeks + ' weeks ago';
    } else if (delta > 86400) {
      const days = Math.floor(delta / 86400);
      return days + ' days ago';
    } else if (delta > 3600) {
      const hours = Math.floor(delta / 3600) % 24;
      return hours + ' hours ago';
    } else if (delta > 60) {
      const minutes = Math.floor(delta / 60) % 60;
      return minutes + ' minutes ago';
    }
  }
}
