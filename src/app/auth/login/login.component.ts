import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { FirebaseService } from 'src/app/services/firebase.service';
import { AuthService } from 'src/app/services/auth.service';
import { AuthGuard } from '../guards/auth.guard';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(
    public afAuth: AngularFireAuth,
    private router: Router,
    private firebaseService: FirebaseService,
    private authService: AuthService,
    public utility: UtilityService
  ) { }
  public email: string = '';
  public password: string = '';
  user: any;
  uid: any;
  userDetails: any;

  ngOnInit() {
    this.utility.showLoader();
    this.authService.isAuth().subscribe((auth) => {
      if (auth) {
        this.uid = auth.uid;
        console.log('User Id Is ' + this.uid);
        this.firebaseService
          .getUserDetail(this.uid)
          .snapshotChanges()
          .subscribe((user) => {

            this.userDetails = [];

            let a = user.payload.toJSON();

            if (!a) {
              this.afAuth.signOut();
              this.router.navigate(['/']);
              return;
            }

            this.userDetails = a
            let _user = JSON.stringify(this.userDetails);
            localStorage.setItem('user', _user);

            this.utility.hideLoader();
            if (this.userDetails.roles.admin == true) {
              console.log('Welcome Super Admin');
              this.onLoginRedirect();
            } else if (this.userDetails.roles.vendor == true) {
              console.log('Welcome Vendor');
              this.onLoginRedirect();
            }
          });
      } else {
        this.utility.hideLoader();
        console.log('Failed');
      }
    });
  }
  onLogin(): void {
    this.authService
      .loginUser(this.email, this.password)
      .then((res) => {

        console.log(res);


      })
      .catch((err) => console.log('err', err.message));
  }

  onLoginGoogle(): void {
    this.authService
      .loginGoogleUser()
      .then((res) => {
        this.onLoginRedirect();
      })
      .catch((err) => console.log('err', err.message));
  }
  onLoginFacebook(): void {
    this.authService
      .loginFacebookUser()
      .then((res) => {
        this.onLoginRedirect();
      })
      .catch((err) => console.log('err', err.message));
  }

  onLogout() {
    this.authService.logoutUser();
  }
  onLoginRedirect(): void {
    this.router.navigate(['tabs/dashboard']);
  }
}

