import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Observable } from 'rxjs';
import { UserInterface } from 'src/app/models/user';
import { finalize } from 'rxjs/operators';
import { VendorInterface } from 'src/app/models/vendor';
import * as firebase from 'firebase';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  isError = false;
  @ViewChild('imageUser') inputImageUser: ElementRef;

  public email = '';
  public password = '';
  public displayName = '';
  public lastName = '';
  public address = '';
  public cardnumber = '';
  public europeResult = '';
  public nation = '';
  public phone = '';
  public role = '';
  constructor(
    private router: Router,
    private authService: AuthService,
    private storage: AngularFireStorage,
    private firebaseService: FirebaseService
  ) { }

  ven_name: any;
  venDeatails: any;
  vendor: any;
  ven_key: any;
  isVendor: boolean = false;
  show: false;
  users: any;
  phoneError = false;
  emailError = false;
  uploadPercent: Observable<number>;
  urlImage: Observable<string>;

  config = {
    displayKey: 'detail_string',
    search: true,
    height: '250px',
    placeholder: 'Vendor',
    limitTo: this.venDeatails?.length,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search Vendor',
    searchOnKey: 'detail_string',
    clearOnSelection: false,
    inputDirection: 'ltr',
  };

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.firebaseService.getUserList().snapshotChanges().subscribe(res => {
      this.users = [];

      res.forEach(user => {
        let a = user.payload.toJSON();
        a['$key'] = user.key;

        this.users.push(a as UserInterface);

      });
    })
  }
  onUpload(e) {
    // console.log('subir', e.target.files[0]);
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    const filePath = `uploads/profile_${id}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();
    task
      .snapshotChanges()
      .pipe(finalize(() => (this.urlImage = ref.getDownloadURL())))
      .subscribe();
  }

  onAddUser() {
    var self = this;
    this.authService.registerUser(this.email, this.password).then((res) => {
      console.log(res);
      let user = res['user'];

      console.log(user);
      let r = this.role.toLowerCase() as string;
      let obj = {};
      obj[r] = true;

      firebase.database().ref('/users').child(user.uid).update({
        displayName: this.displayName,
        ownerId: user.uid,
        email: this.email,
        // address: this.address,
        phone: '+964' + this.phone,
        lastName: this.lastName,
        facebook: false,
        ven_key: this.vendor.$key,
        ven_name: this.vendor.ven_name,
        // cardnumber: "XXXX-XXX",
        // europeResult: this.europeResult,
        // nation: this.nation,
        // facebooks: facebook,
        // instagram: instagram,
        // snapchat: snapchat,
        first: 'true',
        status: 'active',
        // photoURL: this.inputImageUser.nativeElement.value
        roles: obj,
      });

      console.log(this.displayName);
      console.log(this.lastName);
      console.log(this.address);
      console.log(this.phone);

      self.router.navigate(['/tabs/users']);



      // user.updateProfile({
      //   displayName: this.displayName,
      //   photoURL: ""
      // }).then(() => {
      //   self.router.navigate(['users/list']);
      // }).catch((error) => console.log('error', error));
    });
  }

  onSelectVendor() {
    if (this.role === 'VENDOR') {
      this.isVendor = true;
      this.firebaseService
        .getvendor()
        .snapshotChanges()
        .subscribe((vendor) => {
          this.venDeatails = [];

          vendor.forEach((item) => {

            const b = item.payload.toJSON();
            b['$key'] = item.key;
            b['detail_string'] = `${b['ven_name']}  ${b['email']}  ${b['address']}  ${b['phone1']}`;
            console.log(b);
            this.venDeatails.push(b as VendorInterface);
          });
        });
    } else {
      this.isVendor = false;
    }
  }

  onLoginGoogle(): void {
    this.authService
      .loginGoogleUser()
      .then((res) => {
        this.onLoginRedirect();
      })
      .catch((err) => console.log('err', err.message));
  }
  onLoginFacebook(): void {
    this.authService
      .loginFacebookUser()
      .then((res) => {
        this.onLoginRedirect();
      })
      .catch((err) => console.log('err', err.message));
  }

  onLoginRedirect(): void {
    this.router.navigate(['admin/vendors']);
  }

  checkPhoneNo() {
    let index = this.users.findIndex(x => x.phone == this.phone);

    if (index !== -1) {
      if (this.users[index].phone === this.phone) {
        this.phoneError = true;
      }
    } else {
      this.phoneError = false;
    }
  }

  checkEmailNo() {
    let index = this.users.findIndex(x => x.email == this.email);

    if (index !== -1) {
      if (this.users[index].email === this.email) {
        this.emailError = true;
      }
    } else {
      this.emailError = false;
    }
  }
  selectionChanged($event) {
    if ($event.value) {
      this.vendor = $event.value;
    }
  }
}
