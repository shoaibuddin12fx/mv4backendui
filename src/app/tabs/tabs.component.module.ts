import { TabsComponentRoutingModule } from './tabs.component.routing.module';
import { CommonModule } from '@angular/common';
import { TabsComponent } from './tabs.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HeaderComponentModule } from './components/header/header.component.module';
import { SidebarComponentModule } from './components/sidebar/sidebar.component.module';
import { RouterModule } from '@angular/router';
import { FooterComponentModule } from './components/footer/footer.component.module';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { VendorsComponent } from './pages/vendors/vendors.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { ZonesComponent } from './pages/zones/zones.component';
import { DistrictsComponent } from './pages/districts/districts.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { UsersComponent } from './pages/users/users.component';
import { SlidersComponent } from './pages/sliders/sliders.component';
import { NotificationsComponent } from './pages/notifications/notifications.component';
import { ProductsComponent } from './pages/products/products.component';
import { AddCategoryComponent } from './pages/categories/add-category/add-category.component';
import { EditCategoryComponent } from './pages/categories/edit-category/edit-category.component';
import { NgxPubSubModule } from '@pscoped/ngx-pub-sub';
import { FilterPipe } from '../pipes/filter.pipe';
import { OrderDetailComponent } from './pages/orders/order-detail/order-detail.component';
import { AddDistrictComponent } from './pages/districts/add-district/add-district.component';
import { EditDistrictComponent } from './pages/districts/edit-district/edit-district.component';
import { EditProductComponent } from './pages/products/edit-product/edit-product.component';
import { AddProductComponent } from './pages/products/add-product/add-product.component';
import { AddSliderComponent } from './pages/sliders/add-slider/add-slider.component';
import { EditSliderComponent } from './pages/sliders/edit-slider/edit-slider.component';
import { AddVendorComponent } from './pages/vendors/add-vendor/add-vendor.component';
import { EditVendorComponent } from './pages/vendors/edit-vendor/edit-vendor.component';
import { AddZoneComponent } from './pages/zones/add-zone/add-zone.component';
import { EditZoneComponent } from './pages/zones/edit-zone/edit-zone.component';
import { RegisterComponent } from '../auth/register/register.component';
import { ProductDetailComponent } from './pages/products/product-detail/product-detail.component';

import { OrderModule } from 'ngx-order-pipe';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { AboutComponent } from './pages/about/about.component';
import { EditAboutComponent } from './pages/about/edit-about/edit-about.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { EditUserComponent } from './pages/users/edit-user/edit-user.component';



@NgModule({
    entryComponents: [
      
    ],
    declarations: [
      TabsComponent,
      FilterPipe,
      DashboardComponent,
      CategoriesComponent,
      VendorsComponent,
      OrdersComponent,
      ZonesComponent,
      DistrictsComponent,
      SettingsComponent,
      UsersComponent,
      SlidersComponent,
      NotificationsComponent,
      ProductsComponent,
      AddCategoryComponent,
      EditCategoryComponent,
      OrderDetailComponent,
      AddDistrictComponent,
      EditDistrictComponent,
      EditProductComponent,
      AddProductComponent,
      AddSliderComponent,
      EditSliderComponent,
      AddVendorComponent,
      EditVendorComponent,
      AddZoneComponent,
      EditZoneComponent,
      RegisterComponent,
      ProductDetailComponent,
      AboutComponent,
      EditAboutComponent,
      EditUserComponent
    ],
    exports: [
      TabsComponent,
    ],
    imports: [
      CommonModule,
      RouterModule,
      NgxPubSubModule,
      AngularFireModule.initializeApp(environment.firebaseConfig),
      TabsComponentRoutingModule,
      HeaderComponentModule,
      SidebarComponentModule,
      FooterComponentModule,
      FormsModule,
      OrderModule,
      SelectDropDownModule,
      HttpClientModule
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
  })
  export class TabsComponentModule { }