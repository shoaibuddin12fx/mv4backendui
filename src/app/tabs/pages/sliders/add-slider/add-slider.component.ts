import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';
import { CategoryInterface } from 'src/app/models/category';
import { DataService } from 'src/app/services/data.service';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-add-slider',
  templateUrl: './add-slider.component.html',
  styleUrls: ['./add-slider.component.scss']
})
export class AddSliderComponent implements OnInit {

  slider_name: any;
  image?: any;
  status: any = 'active';
  sort: any;
  type: any;

  categories: any;

  constructor(
    private firebaseService: FirebaseService,
    private router: Router,
    private data: DataService,
    private pubsub: NgxPubSubService,
    private utility: UtilityService
  ) { }

  async ngOnInit(): Promise<void> {
    this.categories = await this.data.getCategories();
  }

  async onSliderAddSubmit() {
    if (!this.image) {
      this.utility.presentAlert('Adding Slider Failed Plz Add The Image');
    } else if (this.image) {
      let slider = {
        slider_name: this.slider_name,
        image: this.image,
        status: this.status,
        sort: this.sort,
        created_at: this.firebaseService.getTimeStamp(),
        type: this.type,
      };
      let res = await this.firebaseService.addSlider(slider);
      if (res) {
        this.pubsub.publishEvent('sliderAdded', true);
        this.utility.presentSuccess('Slider Added');
        this.router.navigate(['/tabs/sliders']);
      } else if (!res) {
        this.utility.presentAlert('Adding Slider Failed Plz Fill In All The Fields');
      }
    }
  }
}
