import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { isNgTemplate } from '@angular/compiler';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { Subscription } from 'rxjs';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-sliders',
  templateUrl: './sliders.component.html',
  styleUrls: ['./sliders.component.scss']
})
export class SlidersComponent implements OnInit {
  sliders: any;
  categories: any;
  sliderAdded: Subscription;

  order: string = 'created_at';
  reverse: any = true;
  constructor(
    private dataService: DataService,
    private firebaseService: FirebaseService,
    private pubsub: NgxPubSubService,
    private utility: UtilityService
  ) { }

  async ngOnInit(): Promise<void> {
    this.categories = await this.dataService.getCategories();
    this.sliders = await this.dataService.getSliders();
    this.sliderAdded = this.pubsub.subscribe('sliderAdded', async (data: any) => {
      if (data) {
        this.sliders = await this.dataService.getSliders();
      }
    });
  }

  async deleteSlider($key) {
    let res = await this.utility.presentConfirm();
    if (res) {
      let index = this.sliders.findIndex(x => x.$key === $key);
      if (index !== -1) {
        console.log(this.sliders[index].$key);
        this.firebaseService.deleteSlider(this.sliders[index].$key);
        this.sliders.splice(index, 1);
      }
      this.utility.presentSuccess('Deleted');
    }

  }

  getParentCategory(cat) {

    let par_cat = this.categories.find(x => {
      return x.$key == cat.type
    });

    if (!par_cat) {
      return cat.type
    }

    return par_cat.cat_name;

  }

  async updatesliderStatus($key, status) {
    let obj = {
      id: $key,
      status: status
    };
    this.firebaseService.updateSliderStatus(obj);
    this.sliders = await this.dataService.getSliders();
  }
}
