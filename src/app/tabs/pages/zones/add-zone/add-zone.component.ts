import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-add-zone',
  templateUrl: './add-zone.component.html',
  styleUrls: ['./add-zone.component.scss']
})
export class AddZoneComponent implements OnInit {

  available: any;
  category: any;
  description: any;
  image: any;
  name: any;
  price: any;
  stock: any;
  categories: any;
  percent: any;
  sandbox: any;
  production: any;
  cityName: any;
  status: any;
  shippingfee:any;

  constructor(
    private firebaseService: FirebaseService,
    private router: Router,
    private pubsub: NgxPubSubService,
    private utility: UtilityService
  ) {}

  ngOnInit() {}

  addCityName() {
    if (!this.cityName || !this.status || !this.shippingfee) {
      this.utility.presentAlert('Adding Zone Failed Plz Fill In All The Fields');
    } else {
      let cityName = {
        name: this.cityName,
        status: this.status,
        shippingfee: this.shippingfee,
        created_at: this.firebaseService.getTimeStamp(),
      };
  
      this.firebaseService.addNewCity(cityName);
      this.utility.presentSuccess('Zone Added')
      this.router.navigate(['/tabs/zones']);
    }
  }

}
