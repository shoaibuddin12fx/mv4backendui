import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { CityInterface } from 'src/app/models/city';

@Component({
  selector: 'app-edit-zone',
  templateUrl: './edit-zone.component.html',
  styleUrls: ['./edit-zone.component.scss']
})
export class EditZoneComponent implements OnInit {


  id: any;
  name: any;
  category: any;
  description: any;
  available: any;
  price: any;
  stock: any;
  image: any;
  cityName: any;
  city: any;
  status: any;
  shippingfee: any;

  constructor(
    private firebaseService: FirebaseService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService
  ) {



  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];


    this.firebaseService.getCityDetails(this.id).snapshotChanges().subscribe(city => {
      this.city = [];

      let res = city.payload.toJSON();
      res['$key'] = city.key;

      this.city = res as CityInterface;

      console.log(this.city);

      this.cityName = this.city.name;



      console.log(this.id);

      console.log(this.cityName);



      //	  });
    });

  }

  onCityEditSubmit() {

    let city = {
      name: this.cityName,
      status: this.status,
      shippingfee: this.shippingfee

    }

    this.firebaseService.updateCity(this.id, city);

    this.router.navigate(['/city-configuration']);

  }

}
