import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { AboutInterface } from 'src/app/models/about';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  about: any;

  constructor(
    private firebaseService: FirebaseService,
  ) { }

  ngOnInit(): void {
    this.firebaseService.getAbout().snapshotChanges().subscribe(about => {
      this.about = [];

      about.forEach(item => {
        let a = item.payload.toJSON();
        a['$key'] = item.key;

        console.log(a);
        this.about.push(a as AboutInterface);
      })
    });
  }
}
