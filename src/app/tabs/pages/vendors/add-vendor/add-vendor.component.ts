import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-add-vendor',
  templateUrl: './add-vendor.component.html',
  styleUrls: ['./add-vendor.component.scss']
})
export class AddVendorComponent implements OnInit {

  ven_name: any;
  shopname: any;
  latitude: any;
  longitude: any;
  address: any;
  phone1: any;
  phone2: any;
  email: any;
  logo?: any;
  status: any;
  date: any;


  constructor(
    private firebaseService: FirebaseService,
    private router: Router,
    private pubsub: NgxPubSubService,
    private utility: UtilityService
  ) {}

  ngOnInit() {}

  async onVendorAddSubmit(){
    if (!this.logo) {
      this.utility.presentAlert('Adding Vendor Failed Plz Add An Image')
    } else if (this.logo) {
      let vendor = {
        ven_name: this.ven_name,
        shopname: this.shopname,
        latitude: this.latitude,
        longitude: this.longitude,
        address: this.address,
        phone1: this.phone1,
        phone2: this.phone2,
        email: this.email,
        logo: this.logo,
        date: this.date,
        status: this.status,
        created_at: this.firebaseService.getTimeStamp(),
      };

      let res: any = await this.firebaseService.addVendor(vendor);
      if (res) {
        this.pubsub.publishEvent('vendorAdded', true);
        this.utility.presentSuccess('Vendor Added');
        this.router.navigate(['/tabs/vendors']);
      } else if (!res) {
        this.utility.presentAlert('Adding Vendor Failed Plz Fill In All The Fields');
      }
    }
  }

}
