import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
import { ItemInterface } from 'src/app/models/item';
import { CategoryInterface } from 'src/app/models/category';
import { VendorInterface } from 'src/app/models/vendor';
import { finalize } from 'rxjs/operators';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
  @ViewChild('imageupload', { static: true }) imageupload: ElementRef<HTMLElement>;
  @Input() userUid: string;

  @ViewChild('imageUser') inputImageUser: ElementRef;

  id: any;
  name: any;
  description: any;
  available: any;
  price: any;
  stock: any;
  image: any;
  image_firebase_url: any;
  percent: any;
  saleprice: any;
  sub_category: any;
  vendor: any;
  status: any;
  sort: any;
  feature: any;
  new_arrival: any;
  categories: any;
  vendor_key: any;

  imageUrl: any;

  categoryList: any;
  sub_categoryList: any;
  vendorList: any = [];
  itemFolder: any;

  item: any;
  color: any;
  size: any;
  size_price: any;
  device: any;

  files: Array<any> = [];
  colors: Array<string> = [];
  sizes: Array<Object> = [];
  devices: Array<string> = [];

  isVendor: any;
  user: any;
  isImage = false;


  config = {
    displayKey: 'detail_string',
    search: true,
    height: '250px',
    placeholder: 'Vendor',
    limitTo: this.vendorList?.length,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search Vendor',
    searchOnKey: 'detail_string',
    clearOnSelection: false,
    inputDirection: 'ltr',
  };
  mainCategoryList: any;

  constructor(
    private data: DataService,
    private firebaseService: FirebaseService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private storage: AngularFireStorage,
    private utility: UtilityService
  ) {
    this.itemFolder = 'itemimages';
  }



  uploadPercent: Observable<any>;
  urlImage: Observable<string>;

  async ngOnInit() {
    this.checkVendor();
    this.mainCategoryList = await this.data.getCategories();
    this.categoryList = this.mainCategoryList.filter(x => x.parent_category_id === '');
    const venData: any = await this.data.getVendors();
    this.vendorList = [];
    venData.forEach((vendor) => {
      vendor['detail_string'] = `${vendor['ven_name']}  ${vendor['email']}  ${vendor['address']}  ${vendor['phone1']}`;
      this.vendorList.push(vendor);
    });

    this.id = this.route.snapshot.params['id'];

    this.firebaseService
      .getItemDetails(this.id)
      .snapshotChanges()
      .subscribe((item) => {
        this.item = [];

        let res = item.payload.toJSON();
        res['$key'] = item.key;

        this.item = res as ItemInterface;

        this.name = this.item.name;
        this.description = this.item.description;
        this.available = this.item.available;
        this.price = this.item.price;
        this.saleprice = this.item.saleprice;
        this.stock = this.item.stock;
        this.status = this.item.status;
        this.new_arrival = this.item.new_arrival;
        this.sub_category = this.item.sub_category;
        this.categories = this.item.categories;
        this.percent = this.item.percent;
        this.sort = this.item.sort;
        this.vendor_key = this.item.vendor;
        this.feature = this.item.feature;
        this.saleprice = this.item.saleprice;
        this.image_firebase_url = this.item.image_firebase_url;

        if (this.item.gallery_images) {
          Object.keys(this.item.gallery_images).forEach((key) => {
            let url = this.item.gallery_images[key]['firebase_url'];
            let obj = {
              firebase_url: url,
            }
            console.log(obj);
            this.files.push(obj);
          });
        }

        console.log(this.files);

        if (this.item.size) {
          Object.keys(this.item.size).forEach((key) => {
            this.sizes.push(this.item.size[key]);
          });
        }
        if (this.item.color) {
          Object.keys(this.item.color).forEach((key) => {
            this.colors.push(this.item.color[key]);
          });
        }

        if (this.item.devices) {
          Object.keys(this.item.devices).forEach((key) => {
            this.devices.push(this.item.devices[key]);
          });
        }
        this.sub_categoryList = this.mainCategoryList.filter(x => x.parent_category_id === this.categories);
        let sub_category_obj = this.sub_categoryList.find((y) => y.$key === this.item.sub_category);
        this.sub_category = sub_category_obj.cat_id;

      });

  }

  async onItemEditSubmit() {
    if (
      !this.inputImageUser.nativeElement.value ||
      this.inputImageUser.nativeElement.value == undefined
    ) {
      console.log(this.files);

      let images = await this.getGalleryImagesUrl(this.files);

      console.log(images);
      let item = {
        name: this.name,
        description: this.description,
        available: this.available,
        price: this.price,
        stock: this.stock,
        status: this.status,
        sub_category: this.sub_category,
        categories: this.categories,
        saleprice: this.saleprice,
        vendor: this.vendor_key ? this.vendor_key : this.user.ven_key,
        percent: this.percent,
        sort: this.sort,
        feature: this.feature,
        new_arrival: this.new_arrival,
        image: this.image_firebase_url,
        image_firebase_url: this.image_firebase_url,
        size: this.sizes,
        color: this.colors,
        gallery_images: images,
        devices: this.devices
      };

      this.firebaseService.updateItem(this.id, item);

      this.router.navigate(['/tabs/products']);

    }

    if (this.inputImageUser.nativeElement.value) {

      let images = await this.getGalleryImagesUrl(this.files);

      let item = {
        name: this.name,
        description: this.description,
        available: this.available,
        price: this.price,
        stock: this.stock,
        status: this.status,
        sub_category: this.sub_category,
        categories: this.categories,
        vendor: this.vendor_key ? this.vendor_key : this.user.ven_key,
        percent: this.percent,
        saleprice: this.saleprice,
        sort: this.sort,
        feature: this.feature,
        new_arrival: this.new_arrival,
        image: this.inputImageUser.nativeElement.value,
        image_firebase_url: this.inputImageUser.nativeElement.value,
        size: this.sizes,
        color: this.colors,
        gallery_images: images,
        devices: this.devices
      };

      this.firebaseService.updateItemWithImage(this.id, item);

      this.router.navigate(['/tabs/products']);

    }
  }

  onChange($event) {
    // let file = $event.target.files[0]; //  <--- File Object for future use.
    console.log($event);
    this.image = $event; //  <--- File Object for future use.
  }

  onUpload(e) {
    this.isImage = true
    //  console.log('subir', e.target.files[0]);
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    // const filePath = `uploads/profile`;
    const filePath = `/${this.itemFolder}/${file.name}`;

    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();


    task
      .snapshotChanges()
      .pipe(finalize(() => (this.urlImage = ref.getDownloadURL())))
      .subscribe();

    alert('Please wait for uploading images');

    console.log(ref.getDownloadURL());

    console.log(this.urlImage);
  }

  setParentCategory(cat_id) {
    this.sub_categoryList = this.categoryList.filter(
      (x) => x.parent_category_id == cat_id
    );
  }

  addColor(color) {
    let index = this.colors.findIndex(x => x == color)

    if (index === -1) {
      this.colors.push(color);
      color = undefined;
    } else if (index === 0) {
      return;
    }
  }
  removeColor(i) {
    this.colors.splice(i, 1);
  }

  addSize(size) {
    console.log(size);
    let index = this.sizes.findIndex(x => x == size)
    console.log(index);

    let obj = {
      size: this.size,
      price: this.size_price ? this.size_price : 0,
    }

    if (index === -1) {
      this.sizes.push(obj);
      this.size = undefined;
      console.log(this.sizes);
    } else if (index === 0) {
      return;
    }
  }
  removeSize(i) {
    this.sizes.splice(i, 1);
  }

  addDevice(device) {
    console.log(device);
    let index = this.devices.findIndex(x => x == device)
    console.log(index);

    if (index === -1) {
      this.devices.push(device);
      this.device = undefined;
      console.log(this.devices);
    } else if (index === 0) {
      return;
    }
  }

  removeDevice(i) {
    this.devices.splice(i, 1);
  }

  checkVendor() {
    this.user = JSON.parse(localStorage.getItem('user'));
    if (this.user.roles.admin === true) {
      this.isVendor = false;
    } else if (this.user.roles.vendor === true) {
      this.isVendor = true;
    }
  }

  selectionChanged($event) {
    if ($event.value) {
      this.vendor_key = $event.value.$key;
    }
  }


  openImageUpload() {
    let el: HTMLElement = this.imageupload.nativeElement;
    el.click();
  }


  justDelete(i) {
    this.files.splice(i, 1);
  }


  async imagesSelected($event) {
    let files = $event.target.files;

    console.log(files);
    if (files.length > 5) {
      return this.utility.presentAlert("Only Five Images Allowed")
    } else {
      for (let file of files) {
        console.log(this.files);
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = async (e: any) => {

          file['link'] = e.target.result;
          this.files.push(file);

        };
      }
    }
  }

  getGalleryImagesUrl(files) {

    return new Promise(async (resolve: any) => {
      let urls = [];

      if (files.length > 0) {
        let d = Date.now() + "itemImages";


        for (let file of files) {
          if (file['firebase_url']) {
            let obj = {
              firebase_url: file['firebase_url'],
            }
            urls.push(obj);
          } else {
            let firebase_url = await this.firebaseService.getImagefirebaseUrl(file);
            let obj = {
              firebase_url,
            }
            urls.push(obj);
          }
        }

        console.log(urls);

        resolve(urls);
      }

    })

  }

}
