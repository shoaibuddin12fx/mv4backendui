import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Subscription } from 'rxjs';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { AuthService } from 'src/app/services/auth.service';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {

  products: any;
  vendors: any;
  categories: any;
  isVendor: any;

  subscription: Subscription;
  searchText = '';

  productAdded: Subscription;

  order: string = 'created_at';
  reverse: any = true;
  constructor(
    public dataService: DataService,
    public firebaseService: FirebaseService,
    private pubSub: NgxPubSubService,
    private auth: AuthService,
    private utility: UtilityService
  ) { }

  randomDate(date){
    return date ? new Date(date) : new Date(+(new Date()) - Math.floor(Math.random()*10000000000));
  }

  async ngOnInit(): Promise<void> {
    this.isVendor = await this.auth.checkVendor();
    this.vendors = await this.dataService.getVendors();
    this.categories = await this.dataService.getCategories();
    this.products = await this.dataService.getProducts();
    console.log(this.products);
    this.subscription = this.pubSub.subscribe('onsearch', this.onSearch.bind(this));
    this.productAdded = this.pubSub.subscribe('productAdded', async (data: any) => {
      if (data) {
        this.products = await this.dataService.getProducts();
      }
    });
  }

  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }

  onSearch($event) {
    this.searchText = $event;
  }

  async deleteItem($key) {
    let res = await this.utility.presentConfirm();
    if (res) {
      let index = this.products.findIndex(x => x.$key === $key);
      if (index !== -1) {
        this.firebaseService.deleteItem(this.products[index].$key);
        this.products.splice(index, 1);
      }
      this.utility.presentSuccess('Deleted');
    }
  }

  getCategoryName($key) {
    if ($key && this.categories) {
      let find = this.categories.find((x) => x.$key === $key);
      if (find) {
        return find.cat_name;
      } else {
        return '----';
      }
    }
  }

  getvendorName($key) {
    if ($key && this.vendors) {
      return this.vendors.find((x) => x.$key === $key).ven_name;
    }
  }

  async updateProductStatus($key, status) {
    let obj = {
      id: $key,
      status: status
    };
    this.firebaseService.updateProductStatus(obj);
    this.products = await this.dataService.getProducts();
  }
}
