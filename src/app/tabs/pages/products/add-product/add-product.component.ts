import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CategoryInterface } from 'src/app/models/category';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';
import { VendorInterface } from 'src/app/models/vendor';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  @ViewChild('imageupload', { static: true }) imageupload: ElementRef<HTMLElement>;


  available: any;
  description: any;
  image: any;
  name: any;
  price: any;
  stock: any;
  categories: any;
  percent: any;
  saleprice: any;
  sub_category: any;
  vendor: any;
  status: any;
  sort: any;
  feature: any;
  new_arrival: any;
  color: any;
  size: any;
  size_price: any;
  device: any;

  colors: Array<string> = [];
  sizes: Array<Object> = [];
  devices: Array<string> = [];

  category_details: any;

  categoryList: any;
  mainCategoryList: any;
  sub_categoryList: any;
  vendorList: any = [];

  isVendor: any;
  user: any;

  vendor_key: any;

  files: any = [];

  private CategoryInterface: CategoryInterface[];
  public isAdmin: any = null;

  config = {
    displayKey: 'detail_string',
    search: true,
    height: '250px',
    placeholder: 'Vendor',
    limitTo: this.vendorList?.length,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search Vendor',
    searchOnKey: 'detail_string',
    clearOnSelection: false,
    inputDirection: 'ltr',
  };

  constructor(
    private data: DataService,
    private firebaseService: FirebaseService,
    private router: Router,
    private pubsub: NgxPubSubService,
    private utility: UtilityService
  ) { }
  async ngOnInit(): Promise<void> {
    this.checkVendor();
    this.categoryList = await this.data.getCategories();
    this.mainCategoryList = this.categoryList.filter(x => x.parent_category_id === '');
    const venData: any = await this.data.getVendors();
    this.vendorList = [];
    venData.forEach((vendor) => {
      vendor['detail_string'] = `${vendor['ven_name']}  ${vendor['email']}  ${vendor['address']}  ${vendor['phone1']}`;
      this.vendorList.push(vendor);
    });
  }

  async onItemAddSubmit() {
    if (!this.image) {
      this.utility.presentAlert('Adding Product Failed Plz Add The Image');
    } else if (this.image) {

      let images = await this.getGalleryImagesUrl(this.files);

      let item = {
        available: this.available,
        description: this.description,
        name: this.name,
        price: this.price,
        stock: this.stock ? this.stock : 0,
        categories: this.categories,
        percent: this.percent ? this.percent : 0,
        saleprice: this.saleprice ? this.saleprice : 0,
        sub_category: this.sub_category ? this.sub_category : '',
        vendor: this.vendor_key ? this.vendor_key : this.user.ven_key,
        image: this.image,
        feature: this.feature ? this.feature : '',
        new_arrival: this.new_arrival ? this.new_arrival : '',
        sort: this.sort,
        status: this.status ? this.status : "InActive",
        size: this.sizes,
        color: this.colors,
        created_at: this.firebaseService.getTimeStamp(),
        gallery_images: images,
        devices: this.devices,
      };

      console.log(item);
      let res = await this.firebaseService.addItem(item);
      if (res) {
        this.pubsub.publishEvent('productAdded', true);
        this.utility.presentSuccess('Product Added');
        this.router.navigate(['/tabs/products']);
      } else if (!res) {
        this.utility.presentAlert('Adding Product Failed Plz Fill In All The Fields');
      }
    }

  }

  setParentCategory(cat_id) {
    console.log(cat_id);
    this.sub_categoryList = this.categoryList.filter(
      (x) => x.parent_category_id == cat_id
    );
  }

  addColor(color) {
    console.log(color);
    let index = this.colors.findIndex(x => x == color)
    console.log(index);

    if (index === -1) {
      this.colors.push(color);
      this.color = undefined;
      console.log(this.colors);
    } else if (index === 0) {
      return;
    }
  }

  addDevice(device) {
    console.log(device);
    let index = this.devices.findIndex(x => x == device)
    console.log(index);

    if (index === -1) {
      this.devices.push(device);
      this.device = undefined;
      console.log(this.devices);
    } else if (index === 0) {
      return;
    }
  }

  removeDevice(i) {
    this.devices.splice(i, 1);
  }

  removeColor(i) {
    this.colors.splice(i, 1);
  }

  addSize(size) {
    console.log(size);
    let index = this.sizes.findIndex(x => x == size)
    console.log(index);

    let obj = {
      size: this.size,
      price: this.size_price ? this.size_price : 0,
    }

    if (index === -1) {
      this.sizes.push(obj);
      this.size = undefined;
      console.log(this.sizes);
    } else if (index === 0) {
      return;
    }
  }


  removeSize(i) {
    this.sizes.splice(i, 1);
  }

  checkVendor() {
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user);
    if (this.user.roles.admin === true) {
      this.isVendor = false;
    } else if (this.user.roles.vendor === true) {
      this.isVendor = true;
    }
  }

  selectionChanged($event) {
    if ($event.value) {
      console.log(this.vendor);
      this.vendor_key = $event.value.$key;
    }
  }

  async imagesSelected($event) {
    let files = $event.target.files;

    console.log(files);
    if (files.length > 5) {
      return this.utility.presentAlert("Only Five Images Allowed")
    } else {
      for (let file of files) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = async (e: any) => {

          file['link'] = e.target.result;
          this.files.push(file);

        };
      }
    }
  }

  getGalleryImagesUrl(files) {

    return new Promise(async (resolve: any) => {
      let urls = [];

      if (files.length > 0) {
        let d = Date.now() + "itemImages";

        for (let file of files) {
          let firebase_url = await this.firebaseService.getImagefirebaseUrl(file);
          let obj = {
            firebase_url,
          }
          urls.push(obj);
        }

        console.log(urls);

        resolve(urls);
      }

    })

  }


  openImageUpload() {
    let el: HTMLElement = this.imageupload.nativeElement;
    el.click();
  }


  justDelete(i) {
    this.files.splice(i, 1);
  }
}
