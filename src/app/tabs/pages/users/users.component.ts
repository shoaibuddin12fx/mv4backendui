import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Subscription } from 'rxjs';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {

  users: any;

  subscription: Subscription;
  searchText = '';

  vendors: any;
  constructor(
    public dataService: DataService,
    private pubSub: NgxPubSubService,
    public firebaseService: FirebaseService,
  ) { }

  async ngOnInit(): Promise<void> {
    // this.vendors = await this.dataService.getVendors();
    let users = await this.dataService.getUsers();
    console.log(users);
    await this.filterVendors(users);
    console.log(this.users);
    this.subscription = this.pubSub.subscribe('onsearch', this.onSearch.bind(this));
  }

  filterVendors(users) {
    return new Promise(async resolve => {
      this.users = [];
      for (let user of users) {
        if (user.phone !== undefined || user.email !== undefined) {
          this.users.push(user);
          //     if (user.role === undefined || user.role.vendor === true || user.role.buyer === true) {
          //       this.users.push(user);
          //     }
          //   }
          //   // let vendor = this.vendors.find(x => x.$key === user.ven_key);
          //   // if (vendor) {
          //   //   user['vendor'] = vendor;
          //   //   this.users.push(user);
          //   // }
        }
        resolve();
      }
    });
  }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onSearch($event) {
    this.searchText = $event;
  }

  deleteUser($key) {
    let index = this.users.findIndex(x => x.$key === $key);
    if (index !== -1) {
      console.log(this.users[index].$key);
      this.firebaseService.deleteUser(this.users[index].$key);
      this.users.splice(index, 1);
    }
  }
}
