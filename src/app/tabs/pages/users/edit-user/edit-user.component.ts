import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { VendorInterface } from 'src/app/models/vendor';
import { Router, ActivatedRoute } from '@angular/router';
import { UserInterface } from 'src/app/models/user';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  public id: any;
  public displayName = '';
  public vendor: any;
  public venDetails: any;
  public ven_key: any;
  public user: any;
  
  config = {
    displayKey: 'detail_string',
    search: true,
    height: '250px',
    placeholder: 'Vendor',
    limitTo: this.venDetails?.length,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder: 'Search Vendor',
    searchOnKey: 'detail_string',
    clearOnSelection: false,
    inputDirection: 'ltr',
  };

  constructor(
    private firebaseService: FirebaseService ,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.firebaseService.getUserDetail(this.id).snapshotChanges().subscribe(user => {

      let a = user.payload.toJSON();
      a['$key'] = user.key;

      this.user = a as UserInterface;
      console.log(this.user);

      this.displayName = this.user.displayName;
      this.ven_key = this.user.ven_name;
    })
    this.getVendors();
  }

  async updateVendor() {
    console.log(this.vendor.$key)
    let obj = {
      id: this.id,
      displayName: this.displayName,
      ven_key: this.vendor.$key,
      ven_name: this.vendor.ven_name
    }
    this.firebaseService.updateVendorUser(obj);
  }

  getVendors() {
      this.firebaseService
        .getvendor()
        .snapshotChanges()
        .subscribe((vendor) => {
          this.venDetails = [];

          vendor.forEach((item) => {

            const b = item.payload.toJSON();
            b['$key'] = item.key;
            b['detail_string'] = `${b['ven_name']}  ${b['email']}  ${b['address']}  ${b['phone1']}`;
            console.log(b);
            this.venDetails.push(b as VendorInterface);
          });
        });
  }

  
  selectionChanged($event) {
    if ($event.value) {
      this.vendor = $event.value;
    }
  }
}
