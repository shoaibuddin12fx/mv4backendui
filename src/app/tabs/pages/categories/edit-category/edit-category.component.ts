import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
import { CategoryInterface } from 'src/app/models/category';
import { RestaurantInterface } from 'src/app/models/restaurant';
import { finalize } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {

  id: any;
  image?: string;

  imageUrl: any;
  categories: any;
  firebase_url: any;
  categoryFolder: any;
  category: any;

  cat_id: any;
  cat_name: any;
  res_name: any;
  parent_categories: any;
  status: any = 'active';

  parent_category_id: any;
  isImage = false;

  constructor(
    private firebaseService: FirebaseService,
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private storage: AngularFireStorage,
    private data: DataService
  ) {
    this.categoryFolder = 'categoryimages';
  }

  @Input() userUid: string;

  @ViewChild('imageUser') inputImageUser: ElementRef;

  uploadPercent: Observable<any>;
  urlImage: Observable<string>;

  async ngOnInit(): Promise<void> {
    this.id = this.route.snapshot.params['id'];

    this.firebaseService
      .getCategoryDetails(this.id)
      .snapshotChanges()
      .subscribe((category) => {
        this.category = [];

        let res = category.payload.toJSON();
        res['$key'] = category.key;


        this.category = res as CategoryInterface;

        this.cat_id = this.category.cat_id;
        this.cat_name = this.category.cat_name;
        this.image = this.category.image;
        this.parent_category_id = this.category.parent_category_id;
        this.status = this.category.status;
        this.firebase_url = this.category.firebase_url;

        console.log(this.id);

      });

    this.parent_categories = await this.data.getCategories()
  }

  onCategoryEditSubmit() {
    console.log(this.image);

    if (
      !this.inputImageUser.nativeElement.value ||
      this.inputImageUser.nativeElement.value == undefined
    ) {
      console.log('inside');

      let category = {
        cat_id: this.cat_id,
        cat_name: this.cat_name,
        image: this.firebase_url,
        firebase_url: this.firebase_url,
        status: this.status,
        parent_category_id: this.parent_category_id,
      };

      this.firebaseService.updateCategory(this.id, category);

      this.router.navigate(['/tabs/categories']);
    }

    if (this.inputImageUser.nativeElement.value) {
      console.log('white');

      let category = {
        cat_id: this.cat_id,
        cat_name: this.cat_name,
        image: this.inputImageUser.nativeElement.value,
        status: this.status,
        parent_category_id: this.parent_category_id,
        firebase_url: this.inputImageUser.nativeElement.value,
      };

      this.firebaseService.updateCategoryWithImage(this.id, category);

      this.router.navigate(['/tabs/categories']);
    }
  }

  onChange($event) {
    // let file = $event.target.files[0]; // <--- File Object for future use.
    console.log($event);
    this.image = $event; // <--- File Object for future use.
  }

  onUpload(e) {
    this.isImage = true;
    // console.log('subir', e.target.files[0]);
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    // const filePath = `uploads/profile`;
    const filePath = `/${this.categoryFolder}/${file.name}`;

    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges()

    task.snapshotChanges().pipe(finalize(() => this.urlImage = ref.getDownloadURL())).subscribe();

    alert('Please wait for uploading images');

    console.log(ref.getDownloadURL());

    console.log(this.urlImage);
  }

}
