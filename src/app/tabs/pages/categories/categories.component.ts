import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Subscription } from 'rxjs';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { FirebaseService } from 'src/app/services/firebase.service';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit, OnDestroy  {
  categories: any;

  subscription: Subscription;
  catAdded: Subscription;
  searchText = '';

  order: string = 'created_at';
  reverse: any = true;
  constructor(
    public dataService: DataService,
    private pubSub: NgxPubSubService,
    private firebaseService: FirebaseService,
    private utility: UtilityService
  ) { }

  async ngOnInit(): Promise<void> {
    this.categories = await this.dataService.getCategories();
    this.subscription = this.pubSub.subscribe('onsearch', this.onSearch.bind(this));
    this.catAdded = this.pubSub.subscribe('catAdded' , async (data: any) => {
      if (data) {
        this.categories = await this.dataService.getCategories();
      }
     });
  }

  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }

  onSearch($event){
    this.searchText = $event;
  }

  getParentCategory(cat){
    if(!cat["parent_category_id"]){ 
      return "---"
    }

    if(cat["parent_category_id"] == "" || cat["parent_category_id"] == null ){
      return "---"
    }

    let par_cat = this.categories.find( x => {
      return x.$key == cat.parent_category_id
    });

    if(!par_cat){
      return "---"
    }

    return par_cat.cat_name;

  }

  async deleteCategory($key) {
    let res = await this.utility.presentConfirm();
    if (res) {
      let index = this.categories.findIndex(x => x.$key === $key);
      if (index !== -1) {
        console.log(this.categories[index].$key);
        this.firebaseService.deleteCategory(this.categories[index].$key);
        this.categories.splice(index, 1);
      }
      this.utility.presentSuccess('Deleted');
    }
  }

  async updatecategoryStatus($key, status) {
    let obj = {
      id: $key,
      status: status
    };
    this.firebaseService.updateCategoryStatus(obj);
    this.categories = await this.dataService.getCategories();
  }
}
