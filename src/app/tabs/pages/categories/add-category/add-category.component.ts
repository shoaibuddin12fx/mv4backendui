import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';
import { CityInterface } from 'src/app/models/city';
import { CategoryInterface } from 'src/app/models/category';
import { DataService } from 'src/app/services/data.service';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {

  cat_id: any;
  cat_name: any;
  res_name: any;
  image?: any;
  restaurants: any;
  parent_categories: any;
  status: any = 'active';

  restaurant: any;
  parent_category_id: any;
  cities: any;

  public isAdmin: any = null;

  constructor(
    private firebaseService: FirebaseService,
    private router: Router,
    private data: DataService,
    private pubsub: NgxPubSubService,
    private utility: UtilityService
  ) { }


  async ngOnInit(): Promise<void> {
    this.cities = await this.data.getZones();
    this.parent_categories = await this.data.getCategories();
  }

  async onCategoryAddSubmit() {
    this.parent_category_id = this.parent_category_id ? this.parent_category_id : ""

    if (!this.image) {
      this.utility.presentAlert('Adding Category Failed Plz Add The Image');
    } else if (this.image) {
      const category = {
        cat_id: this.cat_id,
        cat_name: this.cat_name,
        image: this.image,
        status: this.status,
        parent_category_id: this.parent_category_id,
        created_at: this.firebaseService.getTimeStamp(),
      };

      let res = await this.firebaseService.addCategory(category);
      if (res) {
        this.pubsub.publishEvent('catAdded', true);
        this.utility.presentSuccess('Category Added');
        this.router.navigate(['/tabs/categories']);
      } else if (!res) {
        this.utility.presentAlert('Adding Category Failed Plz Fill All The Required Fields');
      }
    }
  }

}
