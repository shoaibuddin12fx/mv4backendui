import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { CityInterface } from 'src/app/models/city';
import { DistrictInterface } from 'src/app/models/district';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-edit-district',
  templateUrl: './edit-district.component.html',
  styleUrls: ['./edit-district.component.scss']
})
export class EditDistrictComponent implements OnInit {

  id: any;
  name: any;
  category: any;
  description: any;
  available: any;
  price: any;
  stock: any;
  image: any;
  districtName: any;
  district: any;
  city: any;
  status: any;

  cities: any;

  constructor(
    private data: DataService,
    private firebaseService: FirebaseService,
    private router: Router, private route: ActivatedRoute,
    private utility: UtilityService,
    private authService: AuthService) {
  }

  async ngOnInit(): Promise<void> {

    console.log('Here is Edit Restaurant Page');


    this.id = this.route.snapshot.params['id'];

    this.cities = await this.data.getZones();

    this.firebaseService.getDistrictDetails(this.id).snapshotChanges().subscribe(district => {
      this.district = [];

      let res = district.payload.toJSON();
      res['$key'] = district.key;

      this.district = res as DistrictInterface;

      console.log(this.district);

      this.districtName = this.district.name;
      this.status = this.district.status
      this.city = this.district.city




      console.log(this.id);
      console.log(this.districtName);

    });

  }

  onDistrictEditSubmit() {
    let city = {
      name: this.districtName,
      status: this.status,
      city: this.city,
    };

    this.firebaseService.updateDistrict(this.id, city);
    this.utility.presentSuccess('District Updated');
    this.router.navigate(['/tabs/districts']);
  }
}
