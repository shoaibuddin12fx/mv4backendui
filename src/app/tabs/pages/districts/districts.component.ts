import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Subscription } from 'rxjs';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { FirebaseService } from 'src/app/services/firebase.service';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-districts',
  templateUrl: './districts.component.html',
  styleUrls: ['./districts.component.scss']
})
export class DistrictsComponent implements OnInit, OnDestroy {

  cities: any;
  districts: any;

  subscription: Subscription;
  searchText = '';
  currentDate = new Date();
  order: string = 'created_at';
  reverse: any = true;
  constructor(
    public dataService: DataService,
    private pubSub: NgxPubSubService,
    private firebaseService: FirebaseService,
    private utility: UtilityService
  ) { }

  async ngOnInit(): Promise<void> {
    this.cities = await this.dataService.getZones();
    this.districts = await this.dataService.getDistricts();
    this.subscription = this.pubSub.subscribe('onsearch', this.onSearch.bind(this));
  }

  getZoneName($key) {
    if ($key && this.cities) {
      return this.cities.find((x) => x.$key === $key).name;
    }
  }

  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }

  onSearch($event){
    this.searchText = $event;
  }

  async deleteDistrict($key) {
    let res = await this.utility.presentConfirm();
    if (res) {
      let index = this.districts.findIndex(x => x.$key === $key);
      if (index !== -1) {
        console.log(this.districts[index].$key);
        this.firebaseService.deleteDistrict(this.districts[index].$key);
        this.districts.splice(index, 1);
      }
      this.utility.presentSuccess('Deleted');
    }
  }

  async updateDistrictsStatus($key, status) {
    let obj = {
      id: $key,
      status: status
    };
    this.firebaseService.updateDistrictStatus(obj);
    this.districts = await this.dataService.getDistricts();
  }
}
