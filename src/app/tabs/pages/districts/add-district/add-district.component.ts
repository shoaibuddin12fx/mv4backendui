import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';
import { CityInterface } from 'src/app/models/city';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-add-district',
  templateUrl: './add-district.component.html',
  styleUrls: ['./add-district.component.scss']
})
export class AddDistrictComponent implements OnInit {

  available: any;
  category: any;
  description: any;
  image: any;
  name: any;
  price: any;
  stock: any;
  categories: any;
  percent: any;
  sandbox: any;
  production: any;
  cities: any;
  status: any;

  districtName: any;
  city: any;

  constructor(
    private firebaseService: FirebaseService,
    private router: Router,
    private data: DataService,
    private pubsub: NgxPubSubService,
    private utility: UtilityService
  ) {}

  async ngOnInit(): Promise<void> {
    this.cities = await this.data.getZones();
  }

  addDistrict() {
    console.log(this.districtName, this.city, this.status)
    if (!this.districtName || !this.city || !this.status) {
      this.utility.presentAlert('District Zone Failed Plz Fill In All The Fields');
    } else {
      let district = {
        name: this.districtName,
        status: this.status,
        city: this.city,
        created_at: this.firebaseService.getTimeStamp(),
      };
  
      this.firebaseService.addNewDistrict(district);
      this.utility.presentSuccess('District Added');
      this.router.navigate(['/tabs/districts']);
    }



  }

}
