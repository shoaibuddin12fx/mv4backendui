import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Subscription } from 'rxjs';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { OrderPipe } from 'ngx-order-pipe';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit, OnDestroy {

  orders: any;
  zones: any;
  districts: any;

  subscription: Subscription;
  searchText = '';

  order: string = 'createdAt';
  reverse: any = true;

  constructor(
    public dataService: DataService,
    private pubSub: NgxPubSubService,
  ) { }

  async ngOnInit(): Promise<void> {
    this.zones = await this.dataService.getZones();
    this.districts = await this.dataService.getDistricts();
    this.orders = await this.dataService.getCurrentUserOrders();
    this.subscription = this.pubSub.subscribe('onsearch', this.onSearch.bind(this));
  }

  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();89
    }
  }

  onSearch($event){
    this.searchText = $event;
  }

  getZoneName($key) {
    if ($key && this.zones) {
      return this.zones.find((x) => x.$key === $key).name;
    }
  }

  getSubZoneName($key) {
    if ($key && this.districts) {
      return this.districts.find((x) => x.$key === $key).name;
    }
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
      console.log(this.reverse);
    }

    this.order = value;
  }
}
