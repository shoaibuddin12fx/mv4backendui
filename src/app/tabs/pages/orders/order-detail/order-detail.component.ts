import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FirebaseService } from 'src/app/services/firebase.service';
import { AuthService } from 'src/app/services/auth.service';
import { NetworkService } from 'src/app/services/network.service';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {

  id: any;
  order: any;
  items: any[] = [];
  isAdmin = false;
  isVendor = false;
  ven_key: any;
  vendor: any;
  index: any;
  isIndex = false;
  status: any;
  constructor(
    public dataService: DataService,
    private route: ActivatedRoute,
    private firebaseService: FirebaseService,
    private auth: AuthService,
    private network: NetworkService,
    private utility: UtilityService
  ) { }

  async ngOnInit(): Promise<void> {

    this.getCurrentUserOrderItems();
    await this.dataService.getVendors();
    await this.dataService.getCategories();
    // this.order = this.dataService.orders.find( x => {
    //   return x.$key == this.id;
    // });

    // console.log(this.order);

    // Object.keys(this.order.items).forEach((key) => {
    //   this.items.push(this.order.items[key]);
    // });

  }

  async getCurrentUserOrderItems() {
    this.id = this.route.snapshot.params["id"];
    console.log(this.id);

    this.vendor = await this.auth.checkVendor();
    console.log(this.vendor);
    if (this.vendor === false) {

      this.isAdmin = true;
      this.firebaseService.getOrderDetail(this.id).on('value', (snapshot) => {
        this.order = snapshot.val();
        console.log(this.order);

        this.items = [];
        this.order.items.forEach(element => {
          this.items.push(element);
        });
      });

    } else if (this.vendor === true) {

      this.isVendor = true;
      this.firebaseService.getOrderDetail(this.id).on('value', async (snapshot) => {
        this.order = snapshot.val();
        console.log(this.order);
        this.items = [];
        this.ven_key = await this.auth.getVendorKey();

        this.index = this.order.vendors.findIndex(x => x.vendor_id === this.ven_key);
        this.isIndex = true;

        this.items = this.order.items.filter(x => x.vendor === this.ven_key);
      });
    }
  }

  getCategoryName($key) {
    if ($key && this.dataService.categories) {
      let find = this.dataService.categories.find((x) => x.$key == $key);
      if (find) {
        return find.cat_name;
      } else {
        return '---';
      }
    }
  }

  getCityName($key) {
    if ($key && this.dataService.zones) {
      return this.dataService.zones.find((x) => x.$key == $key).name;
    }
  }

  getDistrictName($key) {
    if ($key && this.dataService.districts) {
      return this.dataService.districts.find((x) => x.$key == $key).name;
    }
  }

  getVendorName($key) {
    if ($key && this.dataService.vendors) {
      return this.dataService.vendors.find((x) => x.$key == $key).ven_name;
    }
    return '---';
  }

  async updateVendorOrderStatus(item, status: string) {
    let obj = {
      id: this.id,
      index: this.index,
      status: status,
    }
    console.log(status);
    this.firebaseService.updateVendorOrderStatus(obj);

    if (status == "Completed") {
      // then mark the order as completed
      await this.markOrderasComplete(status);
    }

    // fire push for update order
    await this.firePushForUpdateOrder(item, status)
  }

  async updateAdminOrderStatus(status: string) {
    for (let i = 0; i < this.order.vendors.length; i++) {
      let obj = {
        id: this.id,
        index: i,
        status: status
      }
      this.firebaseService.updateVendorOrderStatus(obj);
    }
    // Check if the order is completed
    if (status == "Completed") {
      // then mark the order as completed
      let res = await this.utility.presentConfirmForStatus("You Want To Mark The Order As Complete?");
      if (res) {
        await this.markOrderasComplete(status);
      }
    } 
    // else {
    //   await this.markOrderasComplete("Pending");
    // }

    // then send firebasepush
    await this.firePushForCompleteOrder(status);
  }

  markOrderasComplete(status) {
    return new Promise(resolve => {
      for (let i = 0; i < this.order.vendors.length; i++) {
        if (this.order.vendors[i].status === status) {
          if (i == this.order.vendors.length - 1) {
            let obj = {
              id: this.id,
              status: status,
            }
            this.firebaseService.adminMarkOrderasComplete(obj);
          }
        }

      }
    })
  }
  async firePushForCompleteOrder(status) {
    let str = `Your ${this.id} Order , is now ${status}`
    alert(str);


    console.log(this.order.user_id);
    let token = await this.firebaseService.getUserToken(this.order.user_id);
    console.log(token);

    let obj = {
      to: token,

      notification: {
        body: str,
        title: "Order Updated",
        color: '#000000',
      },
      data: {
        body: str,
        title: "Order Updated",
      }
    };

    let reqOptions = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'key=AAAA8CCr7s8:APA91bHDUJ_Yh4fr3TuAeBSERvlcXa5HqrzG2eNVaNnRfPbL1c_eFDda2U666uIFNxAN0LlLqZkzivgrsR-NEiZ8K1o0p2EEfnC3_1G9ikA4OTqnmi37zPa8m_0t54AWnOTNmBuL0bzJ'
      }
    }
    this.network.sendPushNotification(obj, reqOptions);
  }

  async firePushForUpdateOrder(item, status) {
    let ven_name = this.getVendorName(item.vendor);
    let str = `Your ${item.name} ${item.price} , From ${ven_name}  is now on ${status}`
    alert(str);


    console.log(this.order.user_id);
    let token = await this.firebaseService.getUserToken(this.order.user_id);
    console.log(token);

    let obj = {
      to: token,

      notification: {
        body: str,
        title: "Order Updated",
        color: '#000000',
      },
      data: {
        body: str,
        title: "Order Updated",
      }
    };

    let reqOptions = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'key=AAAA8CCr7s8:APA91bHDUJ_Yh4fr3TuAeBSERvlcXa5HqrzG2eNVaNnRfPbL1c_eFDda2U666uIFNxAN0LlLqZkzivgrsR-NEiZ8K1o0p2EEfnC3_1G9ikA4OTqnmi37zPa8m_0t54AWnOTNmBuL0bzJ'
      }
    }
    this.network.sendPushNotification(obj, reqOptions);





  }

  getItemVendorStatus(vendor) {
    console.log(vendor);
    console.log(this.order.vendors);
    if (this.order.vendors) {
      for (let i = 0; i < this.order.vendors.length; i++) {
        const id = this.order.vendors[i].vendor_id;
        if (id === vendor) {
          return this.order.vendors[i].status;
        }
      }
    }
  }
}
