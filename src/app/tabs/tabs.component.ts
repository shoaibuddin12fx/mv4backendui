import { Component, OnInit } from '@angular/core';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  toggled = false;
  
  constructor(private pubSub: NgxPubSubService,) { }

  ngOnInit(): void {
    this.pubSub.subscribe('togglesidebar', this.toggleSidebar.bind(this));
  }

  toggleSidebar(){
    console.log("open/close sidebar");
    this.toggled = !this.toggled;
  }

}
