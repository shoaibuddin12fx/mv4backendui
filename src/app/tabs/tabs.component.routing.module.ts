import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsComponent } from './tabs.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { OrdersComponent } from './pages/orders/orders.component';
import { VendorsComponent } from './pages/vendors/vendors.component';
import { AuthGuard } from '../auth/guards/auth.guard';
import { LoginComponent } from '../auth/login/login.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { ProductsComponent } from './pages/products/products.component';
import { ZonesComponent } from './pages/zones/zones.component';
import { DistrictsComponent } from './pages/districts/districts.component';
import { SlidersComponent } from './pages/sliders/sliders.component';
import { UsersComponent } from './pages/users/users.component';
import { NotificationsComponent } from './pages/notifications/notifications.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { AddVendorComponent } from './pages/vendors/add-vendor/add-vendor.component';
import { AddCategoryComponent } from './pages/categories/add-category/add-category.component';
import { AddProductComponent } from './pages/products/add-product/add-product.component';
import { AddZoneComponent } from './pages/zones/add-zone/add-zone.component';
import { AddDistrictComponent } from './pages/districts/add-district/add-district.component';
import { AddSliderComponent } from './pages/sliders/add-slider/add-slider.component';
import { EditCategoryComponent } from './pages/categories/edit-category/edit-category.component';
import { EditSliderComponent } from './pages/sliders/edit-slider/edit-slider.component';
import { EditDistrictComponent } from './pages/districts/edit-district/edit-district.component';
import { EditZoneComponent } from './pages/zones/edit-zone/edit-zone.component';
import { EditProductComponent } from './pages/products/edit-product/edit-product.component';
import { EditVendorComponent } from './pages/vendors/edit-vendor/edit-vendor.component';
import { RegisterComponent } from '../auth/register/register.component';
import { OrderDetailComponent } from './pages/orders/order-detail/order-detail.component';
import { ProductDetailComponent } from './pages/products/product-detail/product-detail.component';
import { AboutComponent } from './pages/about/about.component';
import { EditAboutComponent } from './pages/about/edit-about/edit-about.component';
import { EditUserComponent } from './pages/users/edit-user/edit-user.component';

const tabsroutes: Routes = [
    {
        path: 'tabs',
        component: TabsComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard',
                pathMatch: 'full'
            },
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'orders',
                component: OrdersComponent
            },
            {
                path: 'order-detail/:id',
                component: OrderDetailComponent
            },
            {
                path: 'vendors',
                component: VendorsComponent
            },
            {
                path: 'add-vendor',
                component: AddVendorComponent
            },
            {
                path: 'edit-vendor/:id',
                component: EditVendorComponent
            },
            {
                path: 'categories',
                component: CategoriesComponent,
            },
            {
                path: 'add-category',
                component: AddCategoryComponent
            },
            {
                path: 'edit-category/:id',
                component: EditCategoryComponent
            },
            {
                path: 'products',
                component: ProductsComponent
            },
            {
                path: 'add-product',
                component: AddProductComponent
            },
            {
                path: 'edit-product/:id',
                component: EditProductComponent
            },
            {
                path: 'product-details/:id',
                component: ProductDetailComponent
            },
            {
                path: 'zones',
                component: ZonesComponent
            },
            {
                path: 'add-zones',
                component: AddZoneComponent
            },
            {
                path: 'edit-zone/:id',
                component: EditZoneComponent
            },
            {
                path: 'districts',
                component: DistrictsComponent
            },
            {
                path: 'add-district',
                component: AddDistrictComponent
            },
            {
                path: 'edit-district/:id',
                component: EditDistrictComponent
            },
            {
                path: 'sliders',
                component: SlidersComponent
            },
            {
                path: 'add-slider',
                component: AddSliderComponent
            },
            {
                path: 'edit-slider/:id',
                component: EditSliderComponent
            },
            {
                path: 'users',
                component: UsersComponent
            },
            {
                path: 'notifications',
                component: NotificationsComponent
            },
            {
                path: 'settings',
                component: SettingsComponent
            },

            {
                path: 'register',
                component: RegisterComponent
            },
            {
                path: 'about',
                component: AboutComponent
            },
            {
                path: 'edit-about',
                component: EditAboutComponent
            },
            {
                path: 'edit-user/:id',
                component: EditUserComponent
            }
        ],
        canActivate: [AuthGuard]

    },
    {
        path: '',
        redirectTo: 'tabs',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: LoginComponent
    },

];

@NgModule({
    imports: [RouterModule.forChild(tabsroutes)],
    exports: [RouterModule]
})
export class TabsComponentRoutingModule { }