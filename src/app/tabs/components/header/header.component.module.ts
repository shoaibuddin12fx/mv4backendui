import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HeaderComponent } from './header.component';
import { RouterModule } from '@angular/router';



@NgModule({
    entryComponents: [
      
    ],
    declarations: [
        HeaderComponent,
    ],
    exports: [
        HeaderComponent,
    ],
    imports: [
      CommonModule,
      RouterModule
    ],
    providers: [
      
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
  })
  export class HeaderComponentModule { }