import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SidebarComponent } from './sidebar.component';
import { RouterModule } from '@angular/router';
import { ListItemComponent } from './list-item/list-item.component';

@NgModule({
    entryComponents: [
      
    ],
    declarations: [
        SidebarComponent,
        ListItemComponent,
    ],
    exports: [
        SidebarComponent,
    ],
    imports: [
      CommonModule,
      RouterModule
    ],
    providers: [
      
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
  })
  export class SidebarComponentModule { }