import { Component, OnInit, Input } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { DataService } from 'src/app/services/data.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {
  count: any = -1;
  private _item: any;
  status = false;
  vendorCount: any = 0;
  hideTab = true;
  isVendor: any = false;

  @Input('Item')
  get Item(): any {
    return this._item;
  };
  set Item(value: any) {
    this._item = value;
    // call count function here with switch case
    console.log(this.isVendor)
    this.calcCount();
  }

  constructor(
    public dataService: DataService,
    public dashboard: DashboardService,
    public authService: AuthService
  ) {
    this.authService.checkVendor().then(res => {
      this.isVendor = res;
    })
  }

  async ngOnInit(): Promise<void> { }


  async calcCount() {
    // Using Switch Statment to Switch Different Casess For Admin And Vendor
    switch (this._item.link) {

      // DashBoard ,Orders and Products will be shown to Vendor...
      // All Items Will be Shown To Admin

      case 'users':
        this.count = await this.dashboard.getUsersCount();
        break;
      case 'vendors':
        this.count = await this.dashboard.getVendorsCount();
        break;
      case 'categories':
        this.count = await this.dashboard.getCategoriesCount();
        break;
      case 'dashboard':
        // different statments for admin and vendor
        let flag = await this.authService.checkVendor();
        if (flag) {
          console.log(localStorage.getItem('user'));
          this.isVendor = false;
          this.dataService.initialize();
        }
        break;
      case 'products':
        let res = await this.authService.checkVendor();
        if (res == false) {
          this.count = await this.dashboard.getProductsCount();
        } else if (res == true) {          
          // getting the lenght of vendor's product from local storage
          
          this.count = localStorage.getItem('productCount');
          this.isVendor = false;
        }
        break;
      case 'orders':
        let response = await this.authService.checkVendor();
        console.log(this.isVendor);
        if (response == false) {
          this.count = await this.dashboard.getOrdersCount();
        } else if (response == true) {
          // getting the lenght of vendor's product from local storage

          this.count = localStorage.getItem('orderCount');
          this.isVendor = false;
        }
        break;
    }
  }
}
