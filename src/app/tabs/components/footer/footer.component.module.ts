import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './footer.component';

@NgModule({
    entryComponents: [
      
    ],
    declarations: [
      FooterComponent,
    ],
    exports: [
      FooterComponent,
    ],
    imports: [
      CommonModule,
      RouterModule
    ],
    providers: [
      
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
  })
  export class FooterComponentModule { }