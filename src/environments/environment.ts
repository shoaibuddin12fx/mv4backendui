// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    // apiKey: "AIzaSyC7OssIThUAOUo-QcjejeQ1-ytjw-QOElA",
    // authDomain: "multi4ionic.firebaseapp.com",
    // databaseURL: "https://multi4ionic.firebaseio.com",
    // projectId: "multi4ionic",
    // storageBucket: "multi4ionic.appspot.com",
    // messagingSenderId: "719400134543"
    apiKey: "AIzaSyB86_-7jSJMl8Ikaz2Gv4_61VATz3ED2_o",
    authDomain: "max-store-app.firebaseapp.com",
    databaseURL: "https://max-store-app.firebaseio.com",
    projectId: "max-store-app",
    storageBucket: "max-store-app.appspot.com",
    messagingSenderId: "997651794177",
    appId: "1:997651794177:web:f4ac216a72f0b5a280b300",
    measurementId: "G-EGFVD1MZLK"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
